﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace KliceHodnotyNaServer
{
    class ProgramSoubory
    {
        static public void PoslatSoubory()
        {
            using (var client = new HttpClient())
            using (var content = new MultipartFormDataContent())
            {
                // Make sure to change API address
                client.BaseAddress = new Uri("http://sd.worklinesluzby.cz/");
                //client.BaseAddress = new Uri("http://localhost:1622/");
                //client.BaseAddress = new Uri("http://localhost:43167/");
                               
                
                // Add first file content 
                var fileContent1 = new ByteArrayContent(File.ReadAllBytes(@"E:\Obrázky\czech.png"));
                fileContent1.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = "czech.png"
                };

                // Add Second file content
                var fileContent2 = new ByteArrayContent(File.ReadAllBytes(@"E:\Obrázky\date.png"));
                fileContent2.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = "date.png"
                };


                content.Add(fileContent1);
                content.Add(fileContent2);

                // Make a call to Web API
                var result = client.PostAsync("/stavebnidennik/api/upload", content).Result;
                //var result = client.PostAsync("/api/upload", content).Result;

                Console.WriteLine(result.StatusCode);
                Console.ReadLine();
            }
        }
    }
}

